from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from django.views.generic import (
    CreateView,
    DetailView,
    ListView,
    UpdateView,
    ListView,
    DeleteView
)
from django.views import View

from blog.forms import (ArticleModelForms, CommentModelForms)
from .models import (Article, Comment)

# from .forms import ArticleModelForm
# from .models import Article


class ArticleListView(ListView):
    template_name = "article/article_list.html"
    queryset = Article.objects.all()


class ArticleCreateView(CreateView):
    template_name = "article/article_create.html"
    form_class = ArticleModelForms

    def form_valid(self, form):
        return super().form_valid(form)


class ArticleDetailView(DetailView):
    template_name = "article/article_detail.html"

    def get_object(self):
        id_ = self.kwargs.get("id")
        return get_object_or_404(Article, id=id_)


class ArticleUpdateView(UpdateView):
    template_name = "article/article_create.html"
    form_class = ArticleModelForms

    def get_object(self):
        id_ = self.kwargs.get("id")
        return get_object_or_404(Article, id=id_)

    def form_valid(self, form):
        return super().form_valid(form)


class ArticleDeleteView(DeleteView):
    template_name = "article/article_delete.html"

    def get_object(self):
        id_ = self.kwargs.get("id")
        return get_object_or_404(Article, id=id_)

    def get_success_url(self):
        return reverse("articles:article-list")
