from django import forms
from blog.models import Article, Comment

class ArticleModelForms(forms.ModelForm):
    class Meta():
        model = Article
        fields = ["title", "text"]

class CommentModelForms(forms.ModelForm):
    class Meta():
        model = Comment
        fields = ["text"]